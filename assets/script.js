let theGames = document.getElementById("theGames");
let theFeatures = document.getElementById("theFeatures");
let navigateToStory = document.getElementById('navigateToStory');
let requirements = document.getElementById('requirements');
let topScore = document.getElementById('topScore');
let footer = document.getElementById('footer');
window.addEventListener('load', () => {
    theGames.hidden = true;
    theFeatures.hidden = true;
    requirements.hidden = true;
    topScore.hidden = true;
    footer.hidden = true;
})

navigateToStory.addEventListener('click', () => {
    theGames.hidden = false;
    theFeatures.hidden = false;
    requirements.hidden = false;
    topScore.hidden = false;
    footer.hidden = false;
    theGames.scrollIntoView({ behavior: "smooth" });
})